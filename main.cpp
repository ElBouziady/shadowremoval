#include "mainwindow.h"
#include <QApplication>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include "opencv2/video/tracking.hpp"
#include <QDir>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    // Parameters
    /*string dir_path_image = "/home/boukary/Desktop/ADM/s2/Prise_16/Camera_Grauche/video_000/";
    int start_index = 360;
    int shift_fg_index = 1;*/

    string dir_path_image = "/home/boukary/Desktop/ADM/shadow-occulsion-image/t1/";
    int start_index = 0;

    // t1
    int shift_fg_index = 17530;
    // t2
    //int shift_fg_index = 1291;
    // t3
    //int shift_fg_index = 8191;
    // t4
    //int shift_fg_index = 9571;
    // t5
    //int shift_fg_index = 21161;
    // t6
    //int shift_fg_index = 1474;


    //opening the image directory
    QDir directory_image(QString(dir_path_image.c_str()));

    if (!directory_image.exists()) {
        cout <<"Directory Image Not Opened !!!"<<endl;
        return 0;
    }


    directory_image.setFilter(QDir::Files);
    directory_image.setSorting(QDir::Name);
    QFileInfoList list_files_image = directory_image.entryInfoList();

    Mat original,foreground,foreground_mod,foreground_final,frame,flow,drawing_contours;
    Mat flowUmat, prevgray;


    for (int i=start_index; i<list_files_image.size();i++){

        /*if (i==420)
            i=945;

        if (i==1000)
            i=1107;

        if (i==1175)
            i=1356;*/


        original=imread(list_files_image[i].absoluteFilePath().toStdString().c_str());
        foreground=imread(format("/home/boukary/Desktop/ADM/shadow-occulsion-image/t1_fg/fg_%d.bmp",i+shift_fg_index));
        foreground_mod = Mat::zeros( foreground.size(), foreground.type() );
        foreground_final = Mat::zeros( foreground.size(), foreground.type() );

        // fill foreground holls

        // Floodfill from point (0, 0)
        Mat tmp = foreground.clone();
        floodFill(tmp, cv::Point(0,0), Scalar(255,255,255));

        // Invert floodfilled image
        bitwise_not(tmp, tmp);

        // Combine the two images to get the foreground.
        foreground = (foreground | tmp);



        cvtColor( original, original, CV_BGR2GRAY );
        cvtColor( original, original, CV_BayerGB2BGR );

        // save original for later
        original.copyTo(frame);

        // just make current frame gray
        cvtColor(frame, frame, COLOR_BGR2GRAY);

        if (prevgray.empty() == false ) {

            // calculate optical flow
            calcOpticalFlowFarneback(prevgray, frame, flowUmat, 0.4, 1, 12, 2, 8, 1.2, 0); // paparmetre optical flow
            // copy Umat container to standard Mat
            flowUmat.copyTo(flow);


            int thresh_flow = 5;
            int c = 1;
            for (int y = 0; y < original.rows; y ++) {
                for (int x = 0; x < original.cols; x ++)
                {
                    Vec3b pixel = foreground.at<Vec3b>(y, x);
                    if (pixel[0]<10)
                        continue;

                    // get the flow from y, x position * 10 for better visibility
                    const Point2f flowatxy = flow.at<Point2f>(y, x) * 5;
                    // draw line at flow direction
                    if (c==10){
                        cv::line(original, Point(x, y), Point(cvRound(x + flowatxy.x), cvRound(y + flowatxy.y)), Scalar(0,0,255));
                        c=1;
                    }
                    else
                        c++;

                    // draw initial point
                    if (cv::norm(cv::Mat(flowatxy))>thresh_flow){
                        circle(original, Point(x, y), 1, Scalar(0, 255, 0), -1);
                        circle(foreground_mod, Point(x, y), 1, Scalar(255, 255, 255), -1);
                    }
                    else
                        circle(original, Point(x, y), 1, Scalar(255, 0, 0), -1);


                }

            }


            // Morphological operations

            int dilate_size = 10;
            int erode_size = 1;
            cv::Mat element_dilate = getStructuringElement( MORPH_RECT,
                                                            Size( 2*dilate_size + 1, 2*dilate_size+1 ),
                                                            Point( dilate_size, dilate_size ) );

            cv::Mat element_erode = getStructuringElement( MORPH_RECT,
                                                           Size( 2*erode_size + 1, 2*erode_size+1 ),
                                                           Point( erode_size, erode_size ) );

            cv::erode(foreground_mod,foreground_mod,element_erode);
            cv::dilate(foreground_mod,foreground_mod,element_dilate);

            // remove foreground contours

            Mat canny_output;
            vector<vector<Point> > contours;
            vector<Vec4i> hierarchy;



            // Find contours
            Mat bin;
            cvtColor(foreground_mod, bin, cv::COLOR_RGB2GRAY);
            threshold(bin, bin,125, 255, cv::THRESH_BINARY);
            findContours( bin, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );


            //imshow("Canny",canny_output);
            if (contours.empty()){
                frame.copyTo(prevgray);
                continue;
            }

            int largest_area=0;
            int largest_contour_index=0;

            for( int j = 0; j< contours.size(); j++ ) // iterate through each contour.
            {
                double a=boundingRect(contours[j]).area();  //  Find the area of contour
                if(a>largest_area){
                    largest_area=a;
                    largest_contour_index=j;                //Store the index of largest contour
                }
            }

            rectangle(foreground_mod,boundingRect(contours[largest_contour_index]),Scalar(0,255,0),2);
            drawContours( foreground_mod, contours,largest_contour_index, Scalar(0,255,255), CV_FILLED );

            // Draw contours
            drawing_contours = Mat::zeros( foreground.size(), CV_8UC3 );
            drawContours( drawing_contours, contours, -1, Scalar(255,255,255));
            drawContours( drawing_contours, contours, largest_contour_index, Scalar(0,255,0));


            // fill previous image again
            frame.copyTo(prevgray);

        }
        else {                        // fill previous image in case prevgray.empty() == true
            frame.copyTo(prevgray);
        }

        /*for(int i=2;i<list.size();i+=4)
        {
            rectangle(original,Rect(list.at(i).toInt(),list.at(i+1).toInt(),list.at(i+2).toInt(),list.at(i+3).toInt()),Scalar(0,255,0),2);
            //imwrite(format("/home/boukary/LocalDataset/hopital/negatives/bg%05d.bmp",j++),raw(Rect(list.at(i).toInt(),list.at(i+1).toInt(),list.at(i+2).toInt(),list.at(i+3).toInt())));

        }*/
        putText(original,list_files_image[i].absoluteFilePath().split("/").last().toStdString().c_str(),cvPoint(24,24),FONT_HERSHEY_COMPLEX_SMALL,0.8,Scalar(0,0,255));
        putText(foreground,format("fg_%d.bmp",i+shift_fg_index),cvPoint(24,24),FONT_HERSHEY_COMPLEX_SMALL,0.8,Scalar(0,0,255));

        if (!original.empty())
            imshow("Image",original);
        if (!foreground.empty())
            imshow("Foreground",foreground);
        /*if (!drawing_contours.empty())
            imshow("Contours",drawing_contours);*/
        if (!foreground_mod.empty())
            imshow("Foreground Mod",foreground_mod);
        /*if (!foreground_final.empty())
            imshow("Foreground Final",foreground_final);*/
        waitKey(0);
    }

    return a.exec();
}
